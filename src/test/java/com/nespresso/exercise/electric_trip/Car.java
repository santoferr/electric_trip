package com.nespresso.exercise.electric_trip;

public class Car {
	
	private int batterySize;
	private int lowSpeedPerformance;
	private int highSpeedPerformance;
	
	public Car(int batterySize, int lowSpeedPerformance, int highSpeedPerformance) {
		super();
		this.batterySize = batterySize;
		this.lowSpeedPerformance = lowSpeedPerformance;
		this.highSpeedPerformance = highSpeedPerformance;
	}

	public int getBatterySize() {
		return batterySize;
	}

	public int getLowSpeedPerformance() {
		return lowSpeedPerformance;
	}

	public int getHighSpeedPerformance() {
		return highSpeedPerformance;
	}
	
	

	
	

}
