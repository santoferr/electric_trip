package com.nespresso.exercise.electric_trip;

public class City {
	
	private String city;
	private Integer chargingCapacity=0;
	
	
	public City(String cityString) {
		
		if (cityString.contains(":")){	
			city=cityString.split(":")[0];
			chargingCapacity=Integer.parseInt(cityString.split(":")[1]);}
		else{
			city=cityString;
		}
	}

	



	public Integer getChargingCapacity() {
		return chargingCapacity;
	}





	public String getCity() {
		return city;
	}





	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		City other = (City) obj;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		return true;
	}





	@Override
	public String toString() {
		return "City [city=" + city + ", chargingCapacity=" + chargingCapacity + "]";
	}

	
	
	
	
}
