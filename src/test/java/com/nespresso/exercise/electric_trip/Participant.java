package com.nespresso.exercise.electric_trip;

public class Participant {
	private int id;
	private Car car;
	private City startCity;
	private City location;
	private Integer charge;
	private Enum<Speed> speed;
	
	public int getId() {
		return id;
	}

	public Participant(int id, Car car, City startCity) {
		super();
		this.id = id;
		this.car = car;
		this.startCity = startCity;
		this.location= new City(startCity.getCity());
		this.charge=this.car.getBatterySize();
	}

	public void setSpeed(Speed speed) {
		this.speed=speed;
		
	}

	public City getLocation() {
		return location;
	}

	public void setLocation(City location) {
		this.location = location;
	}

	public Integer getCharge() {
		return charge;
	}

	public void setCharge(Integer charge) {
		this.charge = charge;
	}

	public Car getCar() {
		return car;
	}
	
	
	


	

}
