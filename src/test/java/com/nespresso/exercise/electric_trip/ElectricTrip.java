package com.nespresso.exercise.electric_trip;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

public class ElectricTrip {

	private List<TripStep> tripSteps;
	private HashMap<Integer, Participant> participants;
	private static int participantIdGenerator = 0;

	public ElectricTrip(String tripString) {
		participants = new HashMap<Integer, Participant>();
		tripSteps = new ArrayList<TripStep>();
		StringTokenizer token = new StringTokenizer(tripString, "-");
		int index = 0;
		TripStep tripStep = null;
		while (token.hasMoreTokens()) {

			if (index % 2 == 0) {
				// it's a city
				if (tripStep == null) {

					City startCity = new City(token.nextToken());

					tripStep = new TripStep();

					tripStep.setStartCity(startCity);
				} else {

					City stopCity = new City(token.nextToken());
					tripStep.setStopCity(stopCity);
					// Appending created tripstep to tripsetp list
					tripSteps.add(tripStep);

					tripStep = new TripStep();
					tripStep.setStartCity(stopCity);

				}

			} else {
				// it's a distance

				Integer distance = Integer.parseInt(token.nextToken());
				tripStep.setDistance(distance);

			}

			index++;

		}

	}

	public int startTripIn(String cityName, int batterySize, int lowSpeedPerformance, int highSpeedPerformance) {

		Car participantCar = new Car(batterySize, lowSpeedPerformance, highSpeedPerformance);
		City startCity = new City(cityName);
		Participant participant = new Participant(++participantIdGenerator, participantCar, startCity);
		participants.put(participantIdGenerator, participant);

		return participant.getId();
	}

	public String chargeOf(int participantId) {
		Participant participant = getParticipant(participantId);
		double charge = participant.getCharge();

		Long percentCharge = Math.round(charge * 100 / participant.getCar().getBatterySize());
		StringBuilder chargeString = new StringBuilder();
		chargeString.append(percentCharge.intValue()).append("%");
		return chargeString.toString();
	}

	public void charge(int participantId, int hoursOfCharge) {
		Participant participant = getParticipant(participantId);

		// get Current City
		City currentLocation = participant.getLocation();
		int charge = currentLocation.getChargingCapacity() * hoursOfCharge;
		if (charge > participant.getCar().getBatterySize()) {
			charge = participant.getCar().getBatterySize();
		}

		participant.setCharge(charge);

		setParticipant(participantId, participant);

	}

	public void move(int participantId, Speed speed) {
		Participant participant = getParticipant(participantId);
		participant.setSpeed(speed);


		// keep moving until the battery can handle it (charging station to be
		// managed later)

		boolean nextStep = false;

		do {

			// find next location

			nextStep = false;

			TripStep tripStep = getTripStep(participant.getLocation());

			if (tripStep != null) {

				Integer speedConsumption = participant.getCar().getHighSpeedPerformance();
				if (speed.equals(Speed.LOW))
					speedConsumption = participant.getCar().getLowSpeedPerformance();

				// check if the car can reach the next city with current battery
				// level
				if ((participant.getCharge() * speedConsumption) >= tripStep.getDistance()) {

					participant.setLocation(tripStep.getStopCity());
					nextStep = true;
					// battery will decrease
					participant.setCharge(participant.getCharge() - (tripStep.getDistance() / speedConsumption));
				}

			}

		} while (nextStep);

		setParticipant(participantId, participant);

	}

	private TripStep getTripStep(City city) {
		for (TripStep tripStep : tripSteps) {
			if (tripStep.getStartCity().equals(city))
				return tripStep;
		}
		return null;
	}

	public void go(int participantId) {

		move(participantId, Speed.LOW);
	}

	public void sprint(int participantId) {
		move(participantId, Speed.HIGH);
	}

	public String locationOf(int participantId) {
		Participant participant = getParticipant(participantId);
		return participant.getLocation().getCity();
	}

	private Participant getParticipant(int participantId) {
		return participants.get(new Integer(participantId));
	}

	private void setParticipant(int participantId, Participant participant) {

		participants.put(new Integer(participantId), participant);
	}

}
