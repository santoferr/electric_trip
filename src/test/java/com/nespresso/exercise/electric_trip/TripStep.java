package com.nespresso.exercise.electric_trip;

public class TripStep {
	
	private City startCity;
	private City stopCity;
	private Integer distance;
	
	
	public void setDistance(Integer distance) {
		this.distance = distance;
	}


	public void setStartCity(City startCity) {
		this.startCity = startCity;
	}


	public void setStopCity(City stopCity) {
		this.stopCity = stopCity;
	}


	public City getStartCity() {
		return startCity;
	}


	public City getStopCity() {
		return stopCity;
	}


	public Integer getDistance() {
		return distance;
	}
	
	


	

	
	
	

}
